---
# dev notes 

## chrome
```
# circleci node containers need install/update chrome.
# also, chrome does not run in non-privileged container on PC
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -O /tmp/chrome.deb
sudo dpkg -i /tmp/chrome.deb
```
## docker build
```
docker run --privileged -it cimg/node:16.17.0-browsers
git clone https://gitlab.com/gudrutis/angular-tour-of-heroes.git toh
cd toh
npm i
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb -O /tmp/chrome.deb
sudo dpkg -i /tmp/chrome.deb
npm run e2e

#docker run -v /var/lib/docker -it --privileged cimg/node:current-browsers
#sudo /etc/init.d/docker start
```
