FROM node:16.17.0 AS build 
LABEL Name="AToH package by Gudrutis" Version=1.0.0
ARG PROD_FLAG=

WORKDIR /build
# copy everything except what is in .dockerignore 
COPY . .
RUN npm i -g @angular/cli 
RUN npm install && ng build $PROD_FLAG
FROM httpd:2.4 as deploy
COPY --from=build /build/dist /usr/local/apache2/htdocs/

# docker run --publish 8080:80 --rm container:TAG